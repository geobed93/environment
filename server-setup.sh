# which vim||{echo "Error vim is not installed"; exit 1; }
# which git||{echo "git is not installed"; exit 1; }
# which sh||{echo "sh is not installed"; exit 1; }

pip3 install --user flake8 yapf isort || echo "warning pip3 is not installed ! Some vim features will not function properly"

git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf

if [ -f ~/.vimrc ]
then
    cp ~/.vimrc ~/.vimrc_old
fi

# cloning the configuration
git clone https://gitlab.com/geobed93/environment.git ~/.environment

# installing vim plugin manager 
cp ~/.environment/vimrc ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# installing vim plugins
git clone https://github.com/jnurmine/Zenburn ~/.vim/bundle/Zenburn
vim +slient +VimEnter +PlugInstall +qall

# installation of oh-my-bash
sh -c "$(wget https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh -O -)"

# bash features addition
echo '
# ignore case for the tab auto-completion
bind "set completion-ignore-case on"

# activate tab to print the completion menu 
bind TAB:menu-complete

# value used as the number of trailing directory components to retain in the command prompt
PROMPT_DIRTRIM=1
export TERM=xterm-256color
ver1=$(tmux -V); ver2="tmux 2.4"; if [ "$ver1" != "$ver2" ]; then ml plgrid/tools/tmux/2.4; fi ' >> ~/.bashrc


echo '
# words which have more than one possible completion cause the matches to be
# listed immediately.
set show-all-if-ambiguous on

# words which have more than one possible completion without any possible
# partial completion (the possible completions don’t share a common prefix)
# cause the matches to be listed immediately
set show-all-if-unmodified on

# menu completion displays the common prefix of the list of possible completions
# (which may be empty) before cycling through the list.
set menu-complete-display-prefix on


# This is incredibly useful for retrieving commands you have used previously and 
# makes a huge difference to your productivity. For instance, to find a
# previous SSH command from a few days ago, simply type “ss” and press up a few times.
"\e[A": history-search-backward
"\e[B": history-search-forward

# The length in characters of the common prefix of a list of possible
# completions that is displayed without modification. 
set completion-prefix-display-length 2
\\$include /etc/input' >> ~/.inputrc

# tmux configuration
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
cp ~/.environment/tmux.conf ~/.tmux.conf
~/.tmux/plugins/tpm/scripts/install_plugins.sh

rm -rf ~/.environment
echo "Please install fzf by executing: ~/.fzf/install"