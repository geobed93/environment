This project contains my linux coding setup.

Personal laptop setup
---------------------

To install lauch the command:

```
git clone https://gitlab.com/geobed93/environment.git ~/.environment
cd ~/.environment
sudo bash local-setup.sh
```

Tested on ubuntu 18.04 LTS using a dockerfile

To build the docker launch the command:
```
 docker build --tag local-setup --file local-setup.sh .
```

To run the docker launch the command:
```
docker run --interactive --tty local-setup
```

Server setup
------------

To install lauch the command:

```
git clone https://gitlab.com/geobed93/environment.git ~/.environment
cd ~/.environment
bash server-setup.sh
```

To build the docker launch the command:
```
 docker build --tag server-setup --file dockerfile-server .
```

To run the docker launch the command:
```
docker run --interactive --tty server-setup
```

Please look at the ~/.tmux.conf and ~/.vimrc files in order to understand the setup.
