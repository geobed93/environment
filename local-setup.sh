# install ZSH
sudo apt-get -y install zsh

# change default shell to ZSH
sudo chsh -s $(which zsh) $(whoami)

# install OH-MY-ZSH
sudo apt-get -y install wget git curl entr
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
source ~/.zshrc
echo "You can change the theme in ~/.zshrc to anyone inlucded in ~/.oh-my-zsh/themes/"
echo "Do not forget to add the plugins you want to use in ~/.zshrc"

# install fzf
git clone https://github.com/junegunn/fzf.git ${ZSH}/custom/plugins/fzf
${ZSH}/custom/plugins/fzf/install --bin
git clone https://github.com/Treri/fzf-zsh.git ${ZSH}/custom/plugins/fzf-zsh
sed -i -r 's/^plugins=\(/plugins=( fzf-zsh/' ~/.zshrc

# install rg
curl -LO https://github.com/BurntSushi/ripgrep/releases/download/0.10.0/ripgrep_0.10.0_amd64.deb
sudo dpkg -i ripgrep_0.10.0_amd64.deb

# install neovim
sudo apt-get -y install software-properties-common
sudo apt-add-repository -y ppa:neovim-ppa/stable
sudo apt-get update
sudo apt-get -y install neovim
sudo apt-get -y install python-dev python-pip python3-dev python3-pip
sudo pip3 install flake8 yapf isort

# sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
# sudo update-alternatives --config vi
# sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
# sudo update-alternatives --config vim
# sudo update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
# sudo update-alternatives --config editor

cp vimrc ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

mkdir -p ~/.config/nvim/

echo "set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc" > ~/.config/nvim/init.vim
nvim -E -c PluginInstall -c q -c q

# install sublime 
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg|sudo apt-key add -
sudo apt-add-repository "deb https://download.sublimetext.com/ apt/stable/"
sudo apt install sublime-text
