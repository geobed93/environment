set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
Plugin 'altercation/vim-colors-solarized'
Plugin 'junegunn/fzf.vim'
Plugin 'powerline/fonts'
Plugin 'mhinz/vim-signify'
Plugin 'Lokaltog/vim-powerline' "powerline
Plugin 'hkupty/nvimux' " brings tmuw to vim
Plugin 'kassio/neoterm' " neoterm :te command to open a terminal
Plugin 'vim-scripts/indentpython.vim' "adds auto ident for python file
"Plugin 'scrooloose/syntastic' " checks the syntax
Plugin 'ajh17/VimCompletesMe' " auto complete 
Plugin 'nvie/vim-flake8' " a synta checker
Plugin 'jnurmine/Zenburn' "low-contrast color scheme for Vim
Plugin 'kien/ctrlp.vim' "fuzzy file search
Plugin 'tmhedberg/SimpylFold' "folding functions
Plugin 'w0rp/ale' "Asynchronous Lint Engine
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

let g:python_host_prog='/usr/bin/python'
let g:python3_host_prog='/usr/bin/python3.6'
let g:loaded_python_provider = 0
let g:loaded_python3_provider = 0 

set ts=4 sw=4

"au BufNewFile,BufRead *.py
"      set autoindent |
"    \ set fileformat=unix 

" UTF-8 Support
set encoding=utf-8
set list listchars=tab:→\ ,trail:·

" you completeMe mod
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

" python with virtualenv support"
" py << EOF
" import os
" import sys
" if 'VIRTUAL_ENV' in os.environ:
"   project_base_dir = os.environ['VIRTUAL_ENV']
"   activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"   execfile(activate_this, dict(__file__=activate_this))
" EOF

let python_highlight_all=1
syntax on

if has('gui_running')
	set background=dark
	colorscheme solarized
else
    colorscheme zenburn
endif


"ignore file"
let NERDTreeIgnore=['\.pyc$', '\~$'] 

set nu "set numbering"
set bs=2 "fix backspace function
set clipboard=unnamed "System clipboard"
syntax on

let g:airline_powerline_fonts = 1
"let g:Powerline_symbols = 'fancy'
"set term=screen-256color
set laststatus=2
set guifont=Liberation\ Mono\ for\ Powerline\ 10
set mouse=a
let NERDTreeMapOpenInTab='<ENTER>'
noremap % V%
" Enable folding
set foldmethod=indent
set foldlevel=99
" Enable folding with the spacebar
nnoremap <space> za

"fix the paste indentation issue 

let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"
inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()
function! XTermPasteBegin()
	set pastetoggle=<Esc>[201~
	set paste
	return ""
endfunction

"map leader to space
"let mapleader = "\<Space>"
let R_tmux_split = 1
map <f2> :TREPLSendLine<cr>j
tnoremap <Esc> <C-\><C-n>


highlight nonascii guibg=Red ctermbg=1 term=standout
au BufReadPost * syntax match nonascii "[^\u0000-\u007F]"
set colorcolumn=80
set expandtab ts=4 sw=4 ai
autocmd InsertEnter,InsertLeave * set cul!

" ale config
let g:ale_sign_column_always = 1
let g:ale_fix_on_save = 1
let g:ale_fixers = {
\   'python': ['yapf','remove_trailing_lines', 'trim_whitespace'],
\   'sh': ['shfmt'],
\   'php': ['trim_whitespace', 'php_cs_fixer'],
\   'javascript': ['trim_whitespace', 'prettier'],
\   'yaml': ['trim_whitespace'],
\   'markdown': ['trim_whitespace']
\}

" Tab navigation like Firefox.
nnoremap <C-p> :tabprevious<CR>
nnoremap <C-n> :tabnext<CR>
nnoremap <C-t> :tabnew<CR>
inoremap <C-p> <Esc>:tabprevious<CR>i
inoremap <C-n> <Esc>:tabnext<CR>i
inoremap <C-t> <Esc>:tabnew<CR>
" in case of nvim terminal mode
tnoremap <C-p> <C-\><C-n> :tabprevious<CR>
tnoremap <C-n> <C-\><C-n> :tabnext<CR>
tnoremap <C-t> <C-\><C-n> :tabnew<CR>

" to add fuzzy find (fzf) to runtimepath
set rtp+=~/.fzf

let g:neoterm_autoinsert=1
set spell
set spelllang=en
set splitright
set autoread

" Autoreload files when changed externally
set updatetime=100
autocmd CursorHold * :checktime

" map the fuzzy find to Ctrl + r
noremap <C-r> :CtrlP<CR>i

" to behave normal when using windows bash
set term=screen-256color

